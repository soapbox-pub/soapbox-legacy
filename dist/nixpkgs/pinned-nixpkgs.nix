# This expression selects a specific revision of Nixpkgs to use,
# for reproducible development environments.
let
  fetcher = { owner, repo, rev, sha256 }: builtins.fetchTarball {
    inherit sha256;
    url = "https://github.com/${owner}/${repo}/archive/${rev}.tar.gz";
  };
in fetcher {
  owner  = "NixOS";
  repo   = "nixpkgs";
  rev    = "18f47ecbac1";  # Nixpkgs unstable 11/21/2019
  sha256 = "16jmqa22av87rh6ivqazf9qfmvi85gj69x72vbglz9pgsxgni72v";
}
