# Use this expression to build the Soapbox production package with
# pinned Nixpkgs.
let
  nixpkgs = import ./pinned-nixpkgs.nix;
  pkgs = import nixpkgs {
    config = { };
    overlays = [
      (self: super: {
        soapbox = self.callPackage ./soapbox { };
      })
    ];
  };
in pkgs.soapbox
