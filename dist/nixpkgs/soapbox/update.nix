{ pkgs, stdenv, lib, writeScript, makeWrapper, yarn2nix-moretea, bundix,
  coreutils, gnused }:
stdenv.mkDerivation rec {
  builder = writeScript "builder" ''
    source $stdenv/setup
    mkdir -p $out/bin
    cp $src/update.sh $out/bin
    patchShebangs $out/bin/update.sh
    wrapProgram $out/bin/update.sh --prefix PATH : ${lib.makeBinPath buildInputs}
  '';
  src = ./.;
  name = "update.sh";
  system = builtins.currentSystem;
  nativeBuildInputs = [ makeWrapper ];
  buildInputs = [ yarn2nix-moretea.yarn2nix bundix coreutils gnused ];
}
