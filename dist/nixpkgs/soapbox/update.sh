#!/bin/bash
set -e

rm -f gemset.nix yarn.nix

# create gemset.nix and update Gemfile.lock
bundix -l

# create yarn.nix and update yarn.lock
yarn2nix > "yarn.nix"
sed "s/https___.*_//g" -i "yarn.nix"
