# This derivation is compatible with Nixpkgs and can be used to build
# a production Soapbox package. Only the source location would
# have to be changed to include this in Nixpkgs.
{ nodejs-slim, yarn2nix-moretea, fetchFromGitHub, bundlerEnv,
  stdenv, yarn, lib, callPackage, imagemagick, ffmpeg, file,
  nix-gitignore, goreman, ... }:

let
  version = "1.0.0rc1";
  unfilteredSrc = ../../../.;
  # Avoid the "dumping very large path" complaint from nix-shell.
  src = nix-gitignore.gitignoreSource [] unfilteredSrc;

  soapbox-gems = bundlerEnv {
    name = "soapbox-gems-${version}";
    inherit version;
    gemfile = unfilteredSrc + "/Gemfile";
    lockfile = unfilteredSrc + "/Gemfile.lock";
    gemset = unfilteredSrc + "/gemset.nix";
  };

  soapbox-js-modules = yarn2nix-moretea.mkYarnPackage {
    pname = "soapbox-modules";
    yarnNix = unfilteredSrc + "/yarn.nix";
    packageJSON = unfilteredSrc + "/package.json";
    # Filter source to avoid unnecessary rebuilds.
    src =
      let
        mkFilter = { filesToInclude, root }: path: type:
          let
            inherit (lib) elem elemAt splitString;
            subpath = elemAt (splitString "${toString root}/" path) 1;
          in (type == "regular" && elem subpath filesToInclude);
      in builtins.filterSource
          (mkFilter {
            filesToInclude = ["package.json" "yarn.lock"];
            root = unfilteredSrc;
          })
          unfilteredSrc;
    inherit version;
  };

  soapbox-assets = stdenv.mkDerivation {
    pname = "soapbox-assets";
    inherit src version;

    buildInputs = [
      soapbox-gems nodejs-slim yarn
    ];

    buildPhase = ''
      cp -r ${soapbox-js-modules}/libexec/soapbox/node_modules "node_modules"
      chmod -R u+w node_modules
      rake assets:precompile
    '';

    installPhase = ''
      mkdir -p $out/public
      cp -r public/assets $out/public
      cp -r public/packs $out/public
    '';
  };

in stdenv.mkDerivation {
  pname = "soapbox";
  inherit src version;

  # This is used to update gemfile.nix and yarn.nix.
  passthru.updateScript = callPackage ./update.nix {};

  # These are used to create a development shell.
  passthru.js-modules = soapbox-js-modules;
  passthru.gems = soapbox-gems;

  buildPhase = ''
    ln -s ${soapbox-js-modules}/libexec/soapbox/node_modules node_modules
    ln -s ${soapbox-assets}/public/assets public/assets
    ln -s ${soapbox-assets}/public/packs public/packs

    patchShebangs bin/
    for b in $(ls ${soapbox-gems}/bin/)
    do
      if [ ! -f bin/$b ]; then
        ln -s ${soapbox-gems}/bin/$b bin/$b
      fi
    done

    rm -rf log
    ln -s /var/log/soapbox log
    ln -s /tmp tmp
  '';
  propagatedBuildInputs = [ imagemagick ffmpeg file soapbox-gems.wrappedRuby ];
  installPhase = ''
    mkdir -p $out
    cp -r * $out/
  '';

  meta = {
    description = "Mastodon-compatible social media software with a focus on custom branding and accessibility";
    homepage = https://soapbox.pub;
    license = lib.licenses.agpl3;
    maintainers = [ ];
  };
}
