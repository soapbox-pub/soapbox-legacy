# frozen_string_literal: true

class AccountPolicy < ApplicationPolicy
  def index?
    staff?
  end

  def show?
    staff?
  end

  def warn?
    staff? && !record.user&.staff?
  end

  def suspend?
    if record.user&.staff?
      false # Staff can't be suspended
    elsif record.local?
      admin? # Only admins can suspend (delete) local accounts
    else
      staff?
    end
  end

  def unsuspend?
    staff?
  end

  def silence?
    staff? && !record.user&.staff?
  end

  def unsilence?
    staff?
  end

  def disable?
    staff?
  end

  def redownload?
    admin?
  end

  def remove_avatar?
    staff?
  end

  def remove_header?
    staff?
  end

  def subscribe?
    admin?
  end

  def unsubscribe?
    admin?
  end

  def memorialize?
    admin? && !record.user&.admin?
  end

  def upgrade?
    !record.is_pro
  end

  def verify?
    admin?
  end

  def edit_pro?
    admin?
  end

  def update_badges?
    admin?
  end
end
