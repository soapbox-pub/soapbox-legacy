# frozen_string_literal: true

module InstanceHelper
  def site_title
    Setting.site_title.blank? ? Rails.configuration.x.local_domain : Setting.site_title
  end

  def site_hostname
    @site_hostname ||= Addressable::URI.parse("//#{Rails.configuration.x.local_domain}").display_uri.host
  end

  def site_logo
    (Rails.cache.fetch('site_uploads/logo') { SiteUpload.find_by(var: 'logo') })&.file&.url || (asset_pack_path 'media/images/logo.svg')
  end
end
