'use strict';

import { STORE_HYDRATE } from '../actions/store';
import { Map as ImmutableMap } from 'immutable';

const initialState = ImmutableMap({
  streaming_api_base_url: null,
  access_token: null,
});

const saveAuthToBrowser = token => {
  if (!token) return;
  localStorage.setItem('soapbox:auth:user', JSON.stringify({
    access_token: token,
    scope: 'read write follow push admin',
    token_type: 'Bearer',
  }));
};

export default function meta(state = initialState, action) {
  switch(action.type) {
  case STORE_HYDRATE:
    saveAuthToBrowser(action.state.getIn(['meta', 'access_token']));
    return state.merge(action.state.get('meta'));
  default:
    return state;
  }
};
