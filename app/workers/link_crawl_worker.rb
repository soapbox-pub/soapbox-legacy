# frozen_string_literal: true

##
# When a link is posted in a status, this worker will fetch the URL
# and generate a preview of the link at the bottom of the status.
#
# See: app/services/fetch_link_card_service.rb

class LinkCrawlWorker
  include Sidekiq::Worker

  sidekiq_options queue: 'pull', retry: 0

  def perform(status_id)
    FetchLinkCardService.new.call(Status.find(status_id))
  rescue ActiveRecord::RecordNotFound
    true
  end
end
