# frozen_string_literal: true

class Settings::ProController < Settings::BaseController
  layout 'admin'

  before_action :authenticate_user!

  def show; end

  def update
    if user_settings.update(user_settings_params.to_h)
      I18n.locale = current_user.locale
      redirect_to settings_pro_path, notice: I18n.t('generic.changes_saved_msg')
    else
      render :show
    end
  end

  private

  def user_settings
    UserSettingsDecorator.new(current_user)
  end

  def user_settings_params
    params.require(:user).permit(
      :setting_pro_display,
      :setting_pro_bg
    )
  end
end
