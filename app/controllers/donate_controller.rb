# frozen_string_literal: true

class DonateController < Settings::BaseController
  include Authorization
  layout 'admin'

  before_action :authenticate_user!
  before_action :verify_sub_owner!, only: [:cancel_confirm, :cancel, :reactivate_confirm, :reactivate, :edit_form, :edit]

  def index
    @customer_id = current_account.get_or_create_stripe

    @monthly_subs = Stripe::Subscription.list({
      customer: @customer_id,
      plan: "plan_monthly_donation",
      expand: ['data.default_payment_method']
    })
  end

  def cancel
    case params[:subscription_action]
    when 'cancel_at_period_end'
      @sub.cancel_at_period_end = true
      @sub.save
      redirect_to :donate, :notice => "Your subscription will be canceled at the next subscription renewal date. Your card will not be charged."
    when 'cancel_immediately'
      invoice = Stripe::Invoice.retrieve(@sub.latest_invoice)
      @sub.delete
      refund = Stripe::Refund.create({
        charge: invoice.charge
      })
      redirect_to :donate, :notice => "Your subscription has been canceled. Your card has been refunded $#{'%.2f' % (refund.amount/100.0)}."
    end
  end

  def reactivate
    @sub.cancel_at_period_end = false
    @sub.save
    redirect_to :donate, :notice => "Your plan has been reactivated. Your card will be charged on your next monthly renewal date."
  end

  def edit
    Stripe::Subscription.update(@sub.id, {
      prorate: false,
      items: [{
        id: @sub.items.data[0].id,
        quantity: (params[:amount].to_f*100).to_i,
      }]
    })

    redirect_to :donate, :notice => "Your plan has been updated. Your card will be charged the new amount at your next monthly renewal date."
  end

  def stripe
    local_domain = Rails.configuration.x.local_domain
    protocol = Rails.configuration.x.use_https ? 'https' : 'http'
    domain = "#{protocol}://#{local_domain}"

    amount = params[:amount] ? params[:amount] : 500
    customer_id = current_account.get_or_create_stripe

    checkout_session = Stripe::Checkout::Session.create({
      payment_method_types: ['card'],
      customer: customer_id,
      subscription_data: {
        items: [{
          plan: 'plan_monthly_donation',
          quantity: amount,
        }],
      },
      success_url: "#{domain}/donate?session_id={CHECKOUT_SESSION_ID}",
      cancel_url: "#{domain}/donate",
    })

    render plain: checkout_session.id
  end

  private

  def verify_sub_owner!
    begin
      @sub = Stripe::Subscription.retrieve({
        id: params[:sub_id],
        expand: [:default_payment_method]
      })
    rescue
      return not_found
    end
    unless current_account.stripe_cus_id == @sub.customer
      return forbidden
    end
  end
end
