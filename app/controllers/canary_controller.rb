# frozen_string_literal: true

class CanaryController < ApplicationController
  layout 'public'

  before_action :set_instance_presenter
  before_action :check_set
  respond_to :html, :text

  private

  def check_set
    not_found if @instance_presenter.warrant_canary.blank?
  end

  def set_instance_presenter
    @instance_presenter = InstancePresenter.new
  end
end
