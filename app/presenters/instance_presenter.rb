# frozen_string_literal: true

class InstancePresenter
  delegate(
    :site_contact_email,
    :site_title,
    :site_short_description,
    :site_description,
    :site_extended_description,
    :site_terms,
    :site_privacy_policy,
    :site_dmca,
    :closed_registrations_message,
    :warrant_canary,
    to: Setting
  )

  def contact_account
    Account.find_local(Setting.site_contact_username.strip.gsub(/\A@/, ''))
  end

  def user_count
    Rails.cache.fetch('user_count') { User.confirmed.joins(:account).merge(Account.without_suspended).count }
  end

  def active_user_count
    Rails.cache.fetch('active_user_count') { Redis.current.pfcount(*(0..3).map { |i| "activity:logins:#{i.weeks.ago.utc.to_date.cweek}" }) }
  end

  def status_count
    Rails.cache.fetch('local_status_count') { Account.local.joins(:account_stat).sum('account_stats.statuses_count') }.to_i
  end

  def domain_count
    Rails.cache.fetch('distinct_domain_count') { Account.distinct.count(:domain) }
  end

  def sample_accounts
    Rails.cache.fetch('sample_accounts', expires_in: 12.hours) { Account.discoverable.popular.limit(3) }
  end

  def version_number
    GabSocial::Version
  end

  def source_url
    GabSocial::Version.source_url
  end

  def thumbnail
    @thumbnail ||= Rails.cache.fetch('site_uploads/thumbnail') { SiteUpload.find_by(var: 'thumbnail') }
  end

  def logo
    @logo ||= Rails.cache.fetch('site_uploads/logo') { SiteUpload.find_by(var: 'logo') }
  end

  def icon
    @icon ||= Rails.cache.fetch('site_uploads/icon') { SiteUpload.find_by(var: 'icon') }
  end

  def mask_icon
    @mask_icon ||= Rails.cache.fetch('site_uploads/mask_icon') { SiteUpload.find_by(var: 'mask_icon') }
  end
end
