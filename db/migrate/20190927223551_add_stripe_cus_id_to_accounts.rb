class AddStripeCusIdToAccounts < ActiveRecord::Migration[5.2]
  def change
    add_column :accounts, :stripe_cus_id, :string
  end
end
