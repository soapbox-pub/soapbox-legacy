# frozen_string_literal: true

##
# Handles Stripe webhook requests with StripeEvent Ruby Gem
# Docs: https://github.com/integrallis/stripe_event

StripeEvent.configure do |events|
  events.all do |event|
    # Log events
    Rails.logger.info "Stripe webhook: #{event.type} event received"
  end

  events.subscribe 'invoice.payment_succeeded' do |event|
    # Give user PRO when payment succeeds
    invoice = event.data.object
    subscription = Stripe::Subscription.retrieve(invoice.subscription)

    # Only handle monthly donations
    return if subscription.plan&.id != "plan_monthly_donation"

    pro_eligible = subscription.quantity >= 500
    account = Account.find_stripe(invoice.customer)
    grace_period = 60*60*48 # 48 hours
    expires_at = Time.at(subscription.current_period_end + grace_period).utc

    if pro_eligible and not account.is_pro
      account.give_pro! expires_at # Give PRO
    elsif not pro_eligible and account.is_pro
      account.end_pro! # Revoke Pro for insufficient amount
    elsif pro_eligible and account.is_pro
      account.pro_expires_at = expires_at
      account.save! # Update Pro expiration
    end
  end

  events.subscribe 'customer.subscription.created' do |event|
    subscription = event.data.object
    if subscription.plan&.id == "plan_monthly_donation"
      # Store stats
      Redis.current.incrby('funding:amount', subscription.quantity)
      Redis.current.incrby('funding:patrons', 1)
    end
  end

  events.subscribe 'customer.subscription.deleted' do |event|
    # Remove PRO when a subscription is deleted
    subscription = event.data.object
    if subscription.plan&.id == "plan_monthly_donation"
      account = Account.find_stripe(subscription.customer)
      account.end_pro! if account.is_pro

      # Store stats
      Redis.current.decrby('funding:amount', subscription.quantity)
      Redis.current.decrby('funding:patrons', 1)
    end
  end
end
