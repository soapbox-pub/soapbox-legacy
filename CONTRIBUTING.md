Contributing
============

Thank you for considering contributing to Soapbox

You can contribute in the following ways:

- Finding and reporting bugs
- Translating the Soapbox interface into various languages
- Contributing code to Soapbox by fixing bugs or implementing features
- Improving the documentation


## Bug reports

Bug reports and feature suggestions can be submitted to Soapbox's [GitLab Issues](https://gitlab.com/soapbox-pub/soapbox/issues) tracker. Please make sure that you are not submitting duplicates, and that a similar report or request has not already been resolved or rejected in the past using the search function. Please also use descriptive, concise titles.
