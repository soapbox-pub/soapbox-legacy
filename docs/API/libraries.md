# Libraries

## Apex (Salesforce)

- [apex-mastodon](https://github.com/tzmfreedom/apex-mastodon)

## C# (.NET Standard)

- [Mastodot](https://github.com/yamachu/Mastodot)
- [Mastonet](https://github.com/glacasa/Mastonet)
- [TootNet](https://github.com/cucmberium/TootNet)
- [mastodon-api-cs](https://github.com/pawotter/mastodon-api-cs)
- [Soapbox.Net](https://github.com/Tlaster/Soapbox.Net)

## C++

- [mastodon-cpp](https://github.com/tastytea/mastodon-cpp)

## Crystal

- [mastodon.cr](https://github.com/decors/mastodon.cr)

## Common Lisp

- [tooter](https://github.com/Shinmera/tooter)

## Elixir

- [hunter](https://github.com/milmazz/hunter)

## Go

- [go-mastodon](https://github.com/mattn/go-mastodon)
- [madon](https://github.com/McKael/madon)

## Haskell

- [hastodon](https://github.com/syucream/hastodon)

## Java

- [mastodon4j](https://github.com/sys1yagi/mastodon4j)

## JavaScript

- [masto.js](https://github.com/neet/masto.js)
- [libodonjs](https://github.com/Zatnosk/libodonjs)

## JavaScript (Browser)

- [mastodon.js](https://github.com/Kirschn/mastodon.js)

## JavaScript (Node.js)

- [node-mastodon](https://github.com/jessicahayley/node-mastodon)
- [mastodon-api](https://github.com/vanita5/mastodon-api)

## Perl

- [Soapbox::Client](https://metacpan.org/pod/Soapbox::Client)

## PHP

- [Soapbox Legacy API for Laravel](https://github.com/kawax/laravel-mastodon-api)
- [Soapbox-api-php](https://github.com/yks118/Soapbox-api-php)
- [Composer based php API wrapper](https://github.com/r-daneelolivaw/mastodon-api-php)
- [SoapboxOAuthPHP](https://github.com/TheCodingCompany/SoapboxOAuthPHP)
- [Phediverse Soapbox Legacy REST Client](https://github.com/phediverse/mastodon-rest)
- [TootoPHP](https://framagit.org/MaxKoder/TootoPHP)
- [oauth2-mastodon](https://github.com/lrf141/oauth2-mastodon)
- [Soapbox Legacy Wordpress API](https://github.com/L1am0/mastodon_wordpress_api)

## Python

- [Soapbox.py](https://github.com/halcy/Soapbox.py)

## R

- [mastodon](https://github.com/ThomasChln/mastodon)

## Ruby

- [mastodon-api](https://gitlab.com/soapbox-pub/soapbox-api)

## Rust

- [mammut](https://github.com/Aaronepower/mammut)
- [elefren](https://github.com/pwoolcoc/elefren)

## Scala

- [scaladon](https://github.com/schwitzerm/scaladon)

## Swift

- [SoapboxKit](https://github.com/ornithocoder/SoapboxKit)
