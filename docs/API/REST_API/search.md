# Search

## GET /api/v2/search

Search for content in accounts, statuses and hashtags.

Returns [Results](../../entities/#results)

### Resource information

|                         |                             |
|-------------------------|-----------------------------|
| Response format         | JSON                        |
| Requires authentication | Yes                         |
| Requires user           | Yes                         |
| Scope                   | ``read`` ``read:search``    |
| Available since         | 2.4.1                       |

### Parameters

|Name|Description|Required|Default|
|----|-----------|:------:|:-----:|
| `q` | The search query | Required ||
| `resolve` | Attempt WebFinger look-up | Optional |false|
| `limit` | Maximum number of results | Optional | 40 |
| `offset` | Offset in the search results | Optional | 0 |
| `following` | Only include accounts the user is following | Optional | false |
