# Domain blocks

## GET /api/v1/domain_blocks

Domains the user has blocked.

Returns array of string.

### Resource information

|                         |                                    |
|-------------------------|------------------------------------|
| Response format         | JSON                               |
| Requires authentication | Yes                                |
| Requires user           | Yes                                |
| Scope                   | ``read`` ``read:blocks`` ``follow``|
| Available since         | 1.4.0                              |

### Parameters

|Name|Description|Required|Default|
|----|-----------|:------:|:-----:|
| `limit` | Maximum number of results | Optional | 40 |

### Pagination

This API returns Link headers containing links to the next and previous page. Since it is using an internal ID, **it is not possible to dynamically generate query parameters to paginate**. You must rely on the Link header.

## POST /api/v1/domain_blocks

Block a domain to hide all public posts from it, all notifications from it, and remove all followers from it.

### Resource information

|                         |                                      |
|-------------------------|--------------------------------------|
| Response format         | JSON                                 |
| Requires authentication | Yes                                  |
| Requires user           | Yes                                  |
| Scope                   | ``write`` ``write:blocks`` ``follow``|
| Available since         | 1.4.0                                |

### Parameters

|Name|Description|Required|
|----|-----------|:------:|
| `domain` | Domain to block| Required |

## DELETE /api/v1/domain_blocks

Remove a domain block.

### Resource information

|                         |                                      |
|-------------------------|--------------------------------------|
| Response format         | JSON                                 |
| Requires authentication | Yes                                  |
| Requires user           | Yes                                  |
| Scope                   | ``write`` ``write:blocks`` ``follow``|
| Available since         | 1.4.0                                |

### Parameters

|Name|Description|Required|
|----|-----------|:------:|
| `domain` | Domain to unblock| Required |
