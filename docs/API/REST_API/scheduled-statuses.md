# Scheduled Statuses

## GET /api/v1/scheduled_statuses

Get scheduled statuses.

Returns array of [ScheduledStatus](../../entities/#scheduledstatus)

### Resource information

|                         |                           |
|-------------------------|---------------------------|
| Response format         | JSON                      |
| Requires authentication | Yes                       |
| Requires user           | Yes                       |
| Scope                   | ``read`` ``read:statuses``|
| Available since         | 2.7.0                     |

## GET /api/v1/scheduled_statuses/:id

Get scheduled status.

Returns [ScheduledStatus](../../entities/#scheduledstatus)

### Resource information

|                         |                           |
|-------------------------|---------------------------|
| Response format         | JSON                      |
| Requires authentication | Yes                       |
| Requires user           | Yes                       |
| Scope                   | ``read`` ``read:statuses``|
| Available since         | 2.7.0                     |

## PUT /api/v1/scheduled_statuses/:id

Update Scheduled status. Only `scheduled_at` can be changed. To change the content, delete it and post a new status.

Returns [ScheduledStatus](../../entities/#scheduledstatus)

### Resource information

|                         |                             |
|-------------------------|-----------------------------|
| Response format         | JSON                        |
| Requires authentication | Yes                         |
| Requires user           | Yes                         |
| Scope                   | ``write`` ``write:statuses``|
| Available since         | 2.7.0                       |

### Parameters

|Name|Description|Required|
|----|-----------|:------:|
| `scheduled_at` | Timestamp string to schedule posting of status (ISO 8601) | Optional |

## DELETE /api/v1/scheduled_statuses/:id

Remove Scheduled status.

### Resource information

|                         |                             |
|-------------------------|-----------------------------|
| Response format         | JSON                        |
| Requires authentication | Yes                         |
| Requires user           | Yes                         |
| Scope                   | ``write`` ``write:statuses``|
| Available since         | 2.7.0                       |
