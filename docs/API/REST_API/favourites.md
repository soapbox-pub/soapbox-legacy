# Favourites

## GET /api/v1/favourites

Statuses the user has favourited.

Returns array of [Status](../../entities/#status)

### Resource information

|                         |                             |
|-------------------------|-----------------------------|
| Response format         | JSON                        |
| Requires authentication | Yes                         |
| Requires user           | Yes                         |
| Scope                   | ``read`` ``read:favourites``|
| Available since         | 0.0.0                       |

### Parameters

|Name|Description|Required|Default|
|----|-----------|:------:|:-----:|
| `limit` | Maximum number of results | Optional | 20 |

### Pagination

This API returns Link headers containing links to the next and previous page. Since it is using an internal ID, **it is not possible to dynamically generate query parameters to paginate**. You must rely on the Link header.

## POST /api/v1/statuses/:id/favourite

Favourite a status.

Returns [Status](../../entities/#status)

### Resource information

|                         |                               |
|-------------------------|-------------------------------|
| Response format         | JSON                          |
| Requires authentication | Yes                           |
| Requires user           | Yes                           |
| Scope                   | ``write`` ``write:favourites``|
| Available since         | 0.0.0                         |

## POST /api/v1/statuses/:id/unfavourite

Undo the favourite of a status.

Returns [Status](../../entities/#status)
