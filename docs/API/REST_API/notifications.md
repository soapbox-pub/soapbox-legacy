# Notifications

## GET /api/v1/notifications

Notifications concerning the user.

Returns array of [Notification](../../entities/#notification)

### Resource information

|                         |                                |
|-------------------------|--------------------------------|
| Response format         | JSON                           |
| Requires authentication | Yes                            |
| Requires user           | Yes                            |
| Scope                   | ``read`` ``read:notifications``|
| Available since         | 0.0.0                          |

### Parameters

|Name|Description|Required|Default|
|----|-----------|:------:|:-----:|
| `max_id` | Return results older than ID | Optional ||
| `since_id` | Return results newer than ID | Optional ||
| `min_id` | Return results immediately newer than ID | Optional ||
| `limit` | Maximum number of results | Optional | 20 |
| `exclude_types` | Array of types to exclude (e.g. `follow`, `favourite`, `reblog`, `mention`) | Optional ||
| `account_id` | Return only notifications sent from given account | Optional ||

### Pagination

This API returns Link headers containing links to the next and previous page. However, the links can also be constructed dynamically using query params and ``id`` values.

## GET /api/v1/notifications/:id

Returns [Notification](../../entities/#notification)

### Resource information

|                         |                                |
|-------------------------|--------------------------------|
| Response format         | JSON                           |
| Requires authentication | Yes                            |
| Requires user           | Yes                            |
| Scope                   | ``read`` ``read:notifications``|
| Available since         | 0.0.0                          |

## POST /api/v1/notifications/:id/dismiss

Delete a single notification from the server.

### Resource information

|                         |                                  |
|-------------------------|----------------------------------|
| Response format         | JSON                             |
| Requires authentication | Yes                              |
| Requires user           | Yes                              |
| Scope                   | ``write`` ``write:notifications``|
| Available since         | 0.0.0                            |

## POST /api/v1/notifications/clear

Delete all notifications from the server.

### Resource information

|                         |                                  |
|-------------------------|----------------------------------|
| Response format         | JSON                             |
| Requires authentication | Yes                              |
| Requires user           | Yes                              |
| Scope                   | ``write`` ``write:notifications``|
| Available since         | 0.0.0                            |

## POST /api/v1/push/subscription

Add a Web Push API subscription to receive notifications. See also: [Web Push API](../../push)

> Each access token can have one push subscription. If you create a new subscription, the old subscription is deleted.

Returns [Push Subscription](../../entities/#push-subscription)

### Resource information

|                         |         |
|-------------------------|---------|
| Response format         | JSON    |
| Requires authentication | Yes     |
| Requires user           | Yes     |
| Scope                   | ``push``|
| Available since         | 2.4.0   |

### Parameters

|Name|Description|Required|
|----|-----------|:------:|
| `subscription[endpoint]` | Endpoint URL that called when notification is happen. | Required |
| `subscription[keys][p256dh]` | User agent public key. Base64 encoded string of public key of ECDH key using 'prime256v1' curve. | Required |
| `subscription[keys][auth]` | Auth secret. Base64 encoded string of 16 bytes of random data. | Required |
| `data[alerts][follow]` | Boolean of whether you want to receive follow notification event. | Optional |
| `data[alerts][favourite]` | Boolean of whether you want to receive favourite notification event. | Optional |
| `data[alerts][reblog]` | Boolean of whether you want to receive reblog notification event. | Optional |
| `data[alerts][mention]` | Boolean of whether you want to receive mention notification event. | Optional |
| `data[alerts][poll]` | Boolean of whether you want to receive poll result notification event. | Optional |

## GET /api/v1/push/subscription

Returns [Push Subscription](../../entities/#push-subscription)

### Resource information

|                         |         |
|-------------------------|---------|
| Response format         | JSON    |
| Requires authentication | Yes     |
| Requires user           | Yes     |
| Scope                   | ``push``|
| Available since         | 2.4.0   |

## PUT /api/v1/push/subscription

Update current Web Push API subscription. Only the `data` part can be updated, e.g. which types of notifications are desired. To change fundamentals, a new subscription must be created instead.

Returns [Push Subscription](../../entities/#push-subscription)

### Resource information

|                         |         |
|-------------------------|---------|
| Response format         | JSON    |
| Requires authentication | Yes     |
| Requires user           | Yes     |
| Scope                   | ``push``|
| Available since         | 2.4.0   |

### Parameters

|Name|Description|Required|
|----|-----------|:------:|
| `data[alerts][follow]` | Boolean of whether you want to receive follow notification event. | Optional |
| `data[alerts][favourite]` | Boolean of whether you want to receive favourite notification event. | Optional |
| `data[alerts][reblog]` | Boolean of whether you want to receive reblog notification event. | Optional |
| `data[alerts][mention]` | Boolean of whether you want to receive mention notification event. | Optional |
| `data[alerts][poll]` | Boolean of whether you want to receive poll result notification event. | Optional |

## DELETE /api/v1/push/subscription

Remove the current Web Push API subscription.

### Resource information

|                         |         |
|-------------------------|---------|
| Response format         | JSON    |
| Requires authentication | Yes     |
| Requires user           | Yes     |
| Scope                   | ``push``|
| Available since         | 2.4.0   |
