# Blocks

## GET /api/v1/blocks

Accounts the user has blocked.

Returns array of [Account](../../entities/#account)

### Resource information

|                         |                           |
|-------------------------|---------------------------|
| Response format         | JSON                      |
| Requires authentication | Yes                       |
| Requires user           | Yes                       |
| Scope                   | ``read:blocks`` ``follow``|
| Available since         | 0.0.0                     |

### Parameters

|Name|Description|Required|Default|
|----|-----------|:------:|:-----:|
| `limit` | Maximum number of results | Optional | 40 |

### Pagination

This API returns Link headers containing links to the next and previous page. Since it is using an internal ID, **it is not possible to dynamically generate query parameters to paginate**. You must rely on the Link header.

## POST /api/v1/accounts/:id/block

Block an account.

Returns [Relationship](../../entities/#relationship)

### Resource information

|                         |                            |
|-------------------------|----------------------------|
| Response format         | JSON                       |
| Requires authentication | Yes                        |
| Requires user           | Yes                        |
| Scope                   | ``write:blocks`` ``follow``|
| Available since         | 0.0.0                      |

## POST /api/v1/accounts/:id/unblock

Unblock an account.

Returns [Relationship](../../entities/#relationship)

### Resource information

|                         |                            |
|-------------------------|----------------------------|
| Response format         | JSON                       |
| Requires authentication | Yes                        |
| Requires user           | Yes                        |
| Scope                   | ``write:blocks`` ``follow``|
| Available since         | 0.0.0                      |
