# Endorsements

## GET /api/v1/endorsements

Accounts the user chose to endorse.

Returns array of [Account](../../entities/#account)

### Resource information

|                         |                          |
|-------------------------|--------------------------|
| Response format         | JSON                     |
| Requires authentication | Yes                      |
| Requires user           | Yes                      |
| Scope                   | ``read`` ``read:account``|
| Available since         | 2.5.0                    |

### Pagination

This API returns Link headers containing links to the next and previous page. Since it is using an internal ID, **it is not possible to dynamically generate query parameters to paginate**. You must rely on the Link header.

## POST /api/v1/accounts/:id/pin

Endorse an account, i.e. choose to feature the account on the user's public profile.

Returns [Relationship](../../entities/#relationship)

### Resource information

|                         |                             |
|-------------------------|-----------------------------|
| Response format         | JSON                        |
| Requires authentication | Yes                         |
| Requires user           | Yes                         |
| Scope                   | ``write`` ``write:accounts``|
| Available since         | 2.5.0                       |

## POST /api/v1/accounts/:id/unpin

Undo endorse of an account.

Returns [Relationship](../../entities/#relationship)

### Resource information

|                         |                             |
|-------------------------|-----------------------------|
| Response format         | JSON                        |
| Requires authentication | Yes                         |
| Requires user           | Yes                         |
| Scope                   | ``write`` ``write:accounts``|
| Available since         | 2.5.0                       |
