# Polls

## GET /api/v1/polls/:id

Returns [Poll](../../entities/#poll)

### Resource information

|                         |                            |
|-------------------------|----------------------------|
| Response format         | JSON                       |
| Requires authentication | No                         |
| Requires user           | No                         |
| Scope                   | ``read`` ``read:statuses`` |
| Available since         | 2.8.0                      |

## POST /api/v1/polls/:id/votes

Vote on a poll.

Returns [Poll](../../entities/#poll)

### Parameters

|Name|Description|Required|
|----|-----------|:------:|
| `choices` | Array of choice indices | Required |

### Resource information

|                         |                             |
|-------------------------|-----------------------------|
| Response format         | JSON                        |
| Requires authentication | Yes                         |
| Requires user           | Yes                         |
| Scope                   | ``write`` ``write:statuses``|
| Available since         | 2.8.0                       |
