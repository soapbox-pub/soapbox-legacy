# Mutes

## GET /api/v1/mutes

Accounts the user has muted.

Returns array of [Account](../../entities/#account)

### Resource information

|                         |                          |
|-------------------------|--------------------------|
| Response format         | JSON                     |
| Requires authentication | Yes                      |
| Requires user           | Yes                      |
| Scope                   | ``read:mutes`` ``follow``|
| Available since         | 0.0.0                    |

### Parameters

|Name|Description|Required|Default|
|----|-----------|:------:|:-----:|
| `limit` | Maximum number of results | Optional | 40 |

### Pagination

This API returns Link headers containing links to the next and previous page. Since it is using an internal ID, **it is not possible to dynamically generate query parameters to paginate**. You must rely on the Link header.

## POST /api/v1/accounts/:id/mute

Mute an account.

Returns [Relationship](../../entities/#relationship)

### Resource information

|                         |                           |
|-------------------------|---------------------------|
| Response format         | JSON                      |
| Requires authentication | Yes                       |
| Requires user           | Yes                       |
| Scope                   | ``write:mutes`` ``follow``|
| Available since         | 0.0.0                     |

### Parameters

|Name|Description|Required|Default|
|----|-----------|:------:|:-----:|
| `notifications` | Whether the mute will mute notifications or not | Optional | true |

## POST /api/v1/accounts/:id/unmute

Unmute an account.

Returns [Relationship](../../entities/#relationship)

### Resource information

|                         |                           |
|-------------------------|---------------------------|
| Response format         | JSON                      |
| Requires authentication | Yes                       |
| Requires user           | Yes                       |
| Scope                   | ``write:mutes`` ``follow``|
| Available since         | 0.0.0                     |

## POST /api/v1/statuses/:id/mute

Mute the conversation the status is part of, to no longer be notified about it.

Returns [Status](../../entities/#status)

### Resource information

|                         |                          |
|-------------------------|--------------------------|
| Response format         | JSON                     |
| Requires authentication | Yes                      |
| Requires user           | Yes                      |
| Scope                   | ``write`` ``write:mutes``|
| Available since         | 1.4.2                    |

## POST /api/v1/statuses/:id/unmute

Unmute the conversation the status is part of.

Returns [Status](../../entities/#status)

### Resource information

|                         |                          |
|-------------------------|--------------------------|
| Response format         | JSON                     |
| Requires authentication | Yes                      |
| Requires user           | Yes                      |
| Scope                   | ``write`` ``write:mutes``|
| Available since         | 1.4.2                    |
