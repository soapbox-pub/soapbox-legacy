# Custom emoji

## GET /api/v1/custom_emojis

Custom emojis that are available on the server.

Returns array of [Emoji](../../entities/#emoji)

### Resource information

|                         |                            |
|-------------------------|----------------------------|
| Response format         | JSON                       |
| Requires authentication | No                         |
| Requires user           | No                         |
| Available since         | 2.0.0                      |
