# Instances

## GET /api/v1/instance

Information about the server.

Returns [Instance](../../entities/#instance)

### Resource information

|                         |         |
|-------------------------|---------|
| Response format         | JSON    |
| Requires authentication | No      |
| Requires user           | No      |
| Available since         | 0.0.0   |
