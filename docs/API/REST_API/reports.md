# Reports

## POST /api/v1/reports

Report an account.

### Resource information

|                         |                            |
|-------------------------|----------------------------|
| Response format         | JSON                       |
| Requires authentication | Yes                        |
| Requires user           | Yes                        |
| Scope                   | ``write`` ``write:reports``|
| Available since         | 1.1.0                      |

### Parameters

|Name|Description|Required|
|----|-----------|:------:|
| `account_id` | The ID of the account to report | Required |
| `status_ids` | The IDs of statuses to report as array | Optional |
| `comment` | Reason for the report (up to 1,000 characters) | Optional |
| `forward` | Whether to forward to the remote admin (in case of a remote account) | Optional |
