# Follow suggestions

## GET /api/v1/suggestions

Accounts the user had past positive interactions with, but is not following yet.

Returns array of [Account](../../entities/#account)

### Resource information

|                         |         |
|-------------------------|---------|
| Response format         | JSON    |
| Requires authentication | Yes     |
| Requires user           | Yes     |
| Scope                   | ``read``|
| Available since         | 2.4.3   |

## DELETE /api/v1/suggestions/:account_id

Remove account from suggestions.

### Resource information

|                         |         |
|-------------------------|---------|
| Response format         | JSON    |
| Requires authentication | Yes     |
| Requires user           | Yes     |
| Scope                   | ``read``|
| Available since         | 2.4.3   |
