# Filters

## GET /api/v1/filters

Text filters the user has configured that potentially must be applied client-side.

Returns array of [Filter](../../entities/#filter)

### Resource information

|                         |                          |
|-------------------------|--------------------------|
| Response format         | JSON                     |
| Requires authentication | Yes                      |
| Requires user           | Yes                      |
| Scope                   | ``read`` ``read:filters``|
| Available since         | 2.4.3                    |

## POST /api/v1/filters

Create a new filter.

Returns [Filter](../../entities/#filter)

### Resource information

|                         |                            |
|-------------------------|----------------------------|
| Response format         | JSON                       |
| Requires authentication | Yes                        |
| Requires user           | Yes                        |
| Scope                   | ``write`` ``write:filters``|
| Available since         | 2.4.3                      |

### Parameters

|Name|Description|Required|
|----|-----------|:------:|
| `phrase` | Keyword or phrase to filter | Required |
| `context` | Array of strings that means filtering context. Each string is one of `home`, `notifications`, `public`, `thread`. At least one context must be specified. | Required |
| `irreversible` | Irreversible filtering will only work in `home` and `notifications` contexts by fully dropping the records. Otherwise, filtering is up to the client. | Optional |
| `whole_word` | Whether to consider word boundaries when matching | Optional |
| `expires_in` | Number that indicates seconds. Filter will be expire in seconds after API processed. Leave blank for no expiration | Optional |

## GET /api/v1/filters/:id

A text filter.

Returns [Filter](../../entities/#filter)

### Resource information

|                         |                          |
|-------------------------|--------------------------|
| Response format         | JSON                     |
| Requires authentication | Yes                      |
| Requires user           | Yes                      |
| Scope                   | ``read`` ``read:filters``|
| Available since         | 2.4.3                    |

## PUT /api/v1/filters/:id

Update a text filter.

Returns [Filter](../../entities/#filter)

### Resource information

|                         |                            |
|-------------------------|----------------------------|
| Response format         | JSON                       |
| Requires authentication | Yes                        |
| Requires user           | Yes                        |
| Scope                   | ``write`` ``write:filters``|
| Available since         | 2.4.3                      |

### Parameters

|Name|Description|Required|
|----|-----------|:------:|
| `phrase` | Keyword or phrase to filter | Required |
| `context` | Array of strings that means filtering context. Each string is one of `home`, `notifications`, `public`, `thread`. At least one context must be specified. | Required |
| `irreversible` | Irreversible filtering will only work in `home` and `notifications` contexts by fully dropping the records. Otherwise, filtering is up to the client. | Optional |
| `whole_word` | Whether to consider word boundaries when matching | Optional |
| `expires_in` | Number that indicates seconds. Filter will be expire in seconds after API processed. Leave blank to not change | Optional |

## DELETE /api/v1/filters/:id

Delete a text filter.

### Resource information

|                         |                            |
|-------------------------|----------------------------|
| Response format         | JSON                       |
| Requires authentication | Yes                        |
| Requires user           | Yes                        |
| Scope                   | ``write`` ``write:filters``|
| Available since         | 2.4.3                      |
