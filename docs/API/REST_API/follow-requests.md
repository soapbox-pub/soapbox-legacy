# Follow requests

## GET /api/v1/follow_requests

Accounts that have requested to follow the user.

Returns array of [Account](../../entities/#account)

### Resource information

|                         |                                     |
|-------------------------|-------------------------------------|
| Response format         | JSON                                |
| Requires authentication | Yes                                 |
| Requires user           | Yes                                 |
| Scope                   | ``read`` ``read:follows`` ``follow``|
| Available since         | 0.0.0                               |

### Parameters

|Name|Description|Required|Default|
|----|-----------|:------:|:-----:|
| `limit` | Maximum number of results | Optional | 40 |

### Pagination

This API returns Link headers containing links to the next and previous page. Since it is using an internal ID, **it is not possible to dynamically generate query parameters to paginate**. You must rely on the Link header.

## POST /api/v1/follow_requests/:id/authorize

Allow the account to follow the user.

### Resource information

|                         |                             |
|-------------------------|-----------------------------|
| Response format         | JSON                        |
| Requires authentication | Yes                         |
| Requires user           | Yes                         |
| Scope                   | ``write:follows`` ``follow``|
| Available since         | 0.0.0                       |

## POST /api/v1/follow_requests/:id/reject

Do not allow the account to follow the user.

### Resource information

|                         |                             |
|-------------------------|-----------------------------|
| Response format         | JSON                        |
| Requires authentication | Yes                         |
| Requires user           | Yes                         |
| Scope                   | ``write:follows`` ``follow``|
| Available since         | 0.0.0                       |
