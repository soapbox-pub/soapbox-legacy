# Entities

- All IDs are encoded as string representations of integers.
    - IDs can be sorted first by size, and then lexically, to produce a chronological ordering of resources.
- All datetimes are in ISO 8601 format
- All HTML strings are sanitized by the server
- All language codes are in ISO 6391 format

## Account

|Attribute|Type|Nullable|Added in|
|---------|-----------|:------:|:------:|
| `id` | String | <i class="fa fa-times" aria-hidden="true"></i> |0.1.0|
| `username` | String | <i class="fa fa-times" aria-hidden="true"></i> |0.1.0|
| `acct` | String | <i class="fa fa-times" aria-hidden="true"></i> |0.1.0|
| `display_name` | String | <i class="fa fa-times" aria-hidden="true"></i> |0.1.0|
| `locked` | Boolean | <i class="fa fa-times" aria-hidden="true"></i> |0.1.0|
| `created_at` | String (Datetime) | <i class="fa fa-times" aria-hidden="true"></i> |0.1.0|
| `followers_count` | Number | <i class="fa fa-times" aria-hidden="true"></i> |0.1.0|
| `following_count` | Number | <i class="fa fa-times" aria-hidden="true"></i> |0.1.0|
| `statuses_count` | Number | <i class="fa fa-times" aria-hidden="true"></i> |0.1.0|
| `note` | String | <i class="fa fa-times" aria-hidden="true"></i> |0.1.0|
| `url` | String (URL) | <i class="fa fa-times" aria-hidden="true"></i> |0.1.0|
| `avatar` | String (URL) | <i class="fa fa-times" aria-hidden="true"></i> |0.1.0|
| `avatar_static` | String (URL) | <i class="fa fa-times" aria-hidden="true"></i> |1.1.2|
| `header` | String (URL) | <i class="fa fa-times" aria-hidden="true"></i> |0.1.0|
| `header_static` | String (URL) | <i class="fa fa-times" aria-hidden="true"></i> |1.1.2|
| `emojis` | Array of [Emoji](#emoji) | <i class="fa fa-times" aria-hidden="true"></i> |2.4.0|
| `moved` | [Account](#account) | <i class="fa fa-check" aria-hidden="true"></i> |2.1.0|
| `fields` | Array of [Hash](#field) | <i class="fa fa-check" aria-hidden="true"></i> |2.4.0|
| `bot` | Boolean | <i class="fa fa-check" aria-hidden="true"></i> |2.4.0|

### Field

|Attribute|Type|Nullable|Added in|
|---------|-----------|:------:|:------:|
| `name` | String | <i class="fa fa-times" aria-hidden="true"></i> |2.4.0|
| `value` | String (HTML) | <i class="fa fa-times" aria-hidden="true"></i> |2.4.0|
| `verified_at` | String (Datetime) | <i class="fa fa-check" aria-hidden="true"></i> |2.6.0|

### Source

|Attribute|Type|Nullable|Added in|
|---------|-----------|:------:|:------:|
| `privacy` | String | <i class="fa fa-check" aria-hidden="true"></i> |1.5.0|
| `sensitive` | Boolean | <i class="fa fa-check" aria-hidden="true"></i> |1.5.0|
| `language` | String (ISO6391) | <i class="fa fa-check" aria-hidden="true"></i> |2.4.2|
| `note` | String | <i class="fa fa-times" aria-hidden="true"></i> |1.5.0|
| `fields` | Array of Hash | <i class="fa fa-times" aria-hidden="true"></i> |2.4.0|

### Token

|Attribute|Type|Nullable|Added in|
|---------|-----------|:------:|:------:|
| `access_token` | String | <i class="fa fa-times" aria-hidden="true"></i> |0.1.0|
| `token_type` | String | <i class="fa fa-times" aria-hidden="true"></i> |0.1.0|
| `scope` | String | <i class="fa fa-times" aria-hidden="true"></i> |0.1.0|
| `created_at` | Number | <i class="fa fa-times" aria-hidden="true"></i> |0.1.0|

## Application

|Attribute|Type|Nullable|Added in|
|---------|-----------|:------:|:------:|
| `name` | String | <i class="fa fa-times" aria-hidden="true"></i> |0.9.9|
| `website` | String (URL) | <i class="fa fa-check" aria-hidden="true"></i> |0.9.9|

## Attachment

|Attribute|Type|Nullable|Added in|
|---------|-----------|:------:|:------:|
| `id` |  String | <i class="fa fa-times" aria-hidden="true"></i> |0.6.0|
| `type` | [String (Enum)](#type) | <i class="fa fa-times" aria-hidden="true"></i> |0.6.0|
| `url` | String (URL) | <i class="fa fa-times" aria-hidden="true"></i> |0.6.0|
| `remote_url` | String (URL) | <i class="fa fa-check" aria-hidden="true"></i> |0.6.0|
| `preview_url` | String (URL) | <i class="fa fa-times" aria-hidden="true"></i> |0.6.0|
| `text_url` | String (URL) | <i class="fa fa-check" aria-hidden="true"></i> |0.6.0|
| `meta` | [Hash](#meta) | <i class="fa fa-check" aria-hidden="true"></i> |1.5.0|
| `description` | String | <i class="fa fa-check" aria-hidden="true"></i> |2.0.0|

### Type

- `unknown`
- `image`
- `gifv`
- `video`

### Meta

May contain subtrees `small` and `original`.

Images may contain `width`, `height`, `size`, `aspect`, while videos (including GIFV) may contain `width`, `height`, `frame_rate`, `duration` and `bitrate`.

There may be another top-level object, `focus` with the coordinates `x` and `y`. These coordinates can be used for smart thumbnail cropping, [see this for reference](https://github.com/jonom/jquery-focuspoint#1-calculate-your-images-focus-point).

## Card

|Attribute|Type|Nullable|Added in|
|---------|-----------|:------:|:------:|
| `url` | String (URL) | <i class="fa fa-times" aria-hidden="true"></i> |1.0.0|
| `title` | String | <i class="fa fa-times" aria-hidden="true"></i> |1.0.0|
| `description` | String | <i class="fa fa-times" aria-hidden="true"></i> |1.0.0|
| `image` | String (URL) | <i class="fa fa-check" aria-hidden="true"></i> |1.0.0|
| `type` | [String (Enum)](#type-1) | <i class="fa fa-times" aria-hidden="true"></i> |1.3.0|
| `author_name` | String | <i class="fa fa-check" aria-hidden="true"></i> |1.3.0|
| `author_url` | String (URL) | <i class="fa fa-check" aria-hidden="true"></i> |1.3.0|
| `provider_name` | String | <i class="fa fa-check" aria-hidden="true"></i> |1.3.0|
| `provider_url` | String (URL) | <i class="fa fa-check" aria-hidden="true"></i> |1.3.0|
| `html` | String (HTML) | <i class="fa fa-check" aria-hidden="true"></i> |1.3.0|
| `width` | Number | <i class="fa fa-check" aria-hidden="true"></i> |1.3.0|
| `height` | Number | <i class="fa fa-check" aria-hidden="true"></i> |1.3.0|

### Type

- `link`
- `photo`
- `video`
- `rich`

## Context

|Attribute|Type|Nullable|Added in|
|---------|-----------|:------:|:------:|
| `ancestors` | Array of [Status](#status) | <i class="fa fa-times" aria-hidden="true"></i> |0.6.0|
| `descendants` | Array of [Status](#status) | <i class="fa fa-times" aria-hidden="true"></i> |0.6.0|

## Emoji

|Attribute|Type|Nullable|Added in|
|---------|-----------|:------:|:------:|
| `shortcode` | String | <i class="fa fa-times" aria-hidden="true"></i> |2.0.0|
| `static_url` | String (URL) | <i class="fa fa-times" aria-hidden="true"></i> |2.0.0|
| `url` | String (URL) | <i class="fa fa-times" aria-hidden="true"></i> |2.0.0|
| `visible_in_picker` | Boolean | <i class="fa fa-times" aria-hidden="true"></i> |2.1.0|

## Error

The most important part of an error response is the HTTP status code. Standard semantics are followed. The body of an error is a JSON object with this structure:

|Attribute|Type|Nullable|Added in|
|---------|-----------|:------:|:------:|
| `error` | String | <i class="fa fa-times" aria-hidden="true"></i> |0.6.0|

## Filter

|Attribute|Type|Nullable|Added in|
|---------|-----------|:------:|:------:|
| `id` | String | <i class="fa fa-times" aria-hidden="true"></i> |2.4.3|
| `phrase` | String | <i class="fa fa-times" aria-hidden="true"></i> |2.4.3|
| `context` | Array of [String (Enum)](#context-1) | <i class="fa fa-times" aria-hidden="true"></i> |2.4.3|
| `expires_at` | String (Datetime) | <i class="fa fa-check" aria-hidden="true"></i> |2.4.3|
| `irreversible` | Boolean | <i class="fa fa-times" aria-hidden="true"></i> |2.4.3|
| `whole_word` | Boolean | <i class="fa fa-times" aria-hidden="true"></i> |2.4.3|

### Context

- `home`
- `notifications`
- `public`
- `thread`

### Implementation notes

If `whole_word` is true , client app should do:

- Define 'word constituent character' for your app. In the official implementation, it's `[A-Za-z0-9_]` in JavaScript, and `[[:word:]]` in Ruby. In Ruby case it's the POSIX character class (Letter | Mark | Decimal_Number | Connector_Punctuation).
- If the phrase starts with a word character, and if the previous character before matched range is a word character, its matched range should be treated to not match.
- If the phrase ends with a word character, and if the next character after matched range is a word character, its matched range should be treated to not match.

Please check `app/javascript/mastodon/selectors/index.js` and `app/lib/feed_manager.rb` in the Soapbox Legacy source code for more details.

## Instance

|Attribute|Type|Nullable|Added in|
|---------|-----------|:------:|:------:|
| `uri` | String | <i class="fa fa-times" aria-hidden="true"></i> |1.1.0|
| `title` | String | <i class="fa fa-times" aria-hidden="true"></i> |1.1.0|
| `description` | String | <i class="fa fa-times" aria-hidden="true"></i> |1.1.0|
| `email` | String | <i class="fa fa-times" aria-hidden="true"></i> |1.1.0|
| `version` | String | <i class="fa fa-times" aria-hidden="true"></i> |1.3.0|#
| `thumbnail` | String (URL) | <i class="fa fa-check" aria-hidden="true"></i> |1.6.1|
| `urls` | [Hash](#urls) | <i class="fa fa-times" aria-hidden="true"></i> |1.4.2|
| `stats` | [Hash](#stats) | <i class="fa fa-times" aria-hidden="true"></i> |1.6.0|
| `languages` | Array of String (ISO 639, Part 1-5) | <i class="fa fa-times" aria-hidden="true"></i> |2.3.0|
| `contact_account` | [Account](#account) | <i class="fa fa-check" aria-hidden="true"></i> |2.3.0|

### URLs

|Attribute|Type|Nullable|Added in|
|---------|-----------|:------:|:------:|
|`streaming_api`| String (URL) | <i class="fa fa-times" aria-hidden="true"></i> |1.4.2|

### Stats

|Attribute|Type|Nullable|Added in|
|---------|-----------|:------:|:------:|
|`user_count`| Number | <i class="fa fa-times" aria-hidden="true"></i> |1.6.0|
|`status_count`| Number | <i class="fa fa-times" aria-hidden="true"></i> |1.6.0|
|`domain_count`| Number | <i class="fa fa-times" aria-hidden="true"></i> |1.6.0|

## List

|Attribute|Type|Nullable|Added in|
|---------|-----------|:------:|:------:|
| `id` | String | <i class="fa fa-times" aria-hidden="true"></i> |2.1.0|
| `title` | String | <i class="fa fa-times" aria-hidden="true"></i> |2.1.0|

## Mention

|Attribute|Type|Nullable|Added in|
|---------|-----------|:------:|:------:|
| `url` | String (URL) | <i class="fa fa-times" aria-hidden="true"></i> |0.6.0|
| `username` | String | <i class="fa fa-times" aria-hidden="true"></i> |0.6.0|
| `acct` | String | <i class="fa fa-times" aria-hidden="true"></i> |0.6.0|
| `id` | String | <i class="fa fa-times" aria-hidden="true"></i> |0.6.0|

## Notification

|Attribute|Type|Nullable|Added in|
|---------|-----------|:------:|:------:|
| `id` | String | <i class="fa fa-times" aria-hidden="true"></i> |0.9.9|
| `type` | [String (Enum)](#type-2) | <i class="fa fa-times" aria-hidden="true"></i> |0.9.9|
| `created_at` | String (Datetime) | <i class="fa fa-times" aria-hidden="true"></i> |0.9.9|
| `account` | [Account](#account) | <i class="fa fa-times" aria-hidden="true"></i> |0.9.9|
| `status` | [Status](#status) | <i class="fa fa-check" aria-hidden="true"></i> |0.9.9|

### Type

- `follow`
- `mention`
- `reblog`
- `favourite`
- `poll`

## Poll

|Attribute|Type|Nullable|Added in|
|---------|-----------|:------:|:------:|
| `id` | String | <i class="fa fa-times" aria-hidden="true"></i> |2.8.0|
| `expires_at` | String (Datetime) | <i class="fa fa-check" aria-hidden="true"></i> |2.8.0|
| `expired` | Boolean | <i class="fa fa-times" aria-hidden="true"></i> |2.8.0|
| `multiple` | Boolean | <i class="fa fa-times" aria-hidden="true"></i> |2.8.0|
| `votes_count` | Number | <i class="fa fa-times" aria-hidden="true"></i> |2.8.0|
| `options` | Array of [Poll option](#poll-option) | <i class="fa fa-times" aria-hidden="true"></i> |2.8.0|
| `voted` | Boolean | <i class="fa fa-check" aria-hidden="true"></i> |2.8.0|

### Poll option

|Attribute|Type|Nullable|Added in|
|---------|-----------|:------:|:------:|
| `title` | String | <i class="fa fa-times" aria-hidden="true"></i> |2.8.0|
| `votes_count` | Number | <i class="fa fa-check" aria-hidden="true"></i> |2.8.0|

## Push subscription

|Attribute|Type|Nullable|Added in|
|---------|-----------|:------:|:------:|
| `id` | String | <i class="fa fa-times" aria-hidden="true"></i> |2.4.0|
| `endpoint` | String (URL) | <i class="fa fa-times" aria-hidden="true"></i> |2.4.0|
| `server_key` | String | <i class="fa fa-times" aria-hidden="true"></i> |2.4.0|
| `alerts` | [Hash](#alerts) | <i class="fa fa-times" aria-hidden="true"></i> |2.4.0|

### Alerts

???

## Relationship

|Attribute|Type|Nullable|Added in|
|---------|-----------|:------:|:------:|
| `id` | String | <i class="fa fa-times" aria-hidden="true"></i> |0.6.0|
| `following` | Boolean | <i class="fa fa-times" aria-hidden="true"></i> |0.6.0|
| `followed_by` | Boolean | <i class="fa fa-times" aria-hidden="true"></i> |0.6.0|
| `blocking` | Boolean | <i class="fa fa-times" aria-hidden="true"></i> |0.6.0|
| `muting` | Boolean | <i class="fa fa-times" aria-hidden="true"></i> |1.1.0|
| `muting_notifications` | Boolean | <i class="fa fa-times" aria-hidden="true"></i> |2.1.0|
| `requested` | Boolean | <i class="fa fa-times" aria-hidden="true"></i> |0.9.9|
| `domain_blocking` | Boolean | <i class="fa fa-times" aria-hidden="true"></i> |1.4.0|
| `showing_reblogs` | Boolean | <i class="fa fa-times" aria-hidden="true"></i> |2.1.0|
| `endorsed` | Boolean | <i class="fa fa-times" aria-hidden="true"></i> |2.5.0|

## Results

|Attribute|Type|Nullable|Added in|
|---------|-----------|:------:|:------:|
| `accounts` | Array of [Account](#account) | <i class="fa fa-times" aria-hidden="true"></i> |1.1.0|
| `statuses` | Array of [Status](#status)  | <i class="fa fa-times" aria-hidden="true"></i> |1.1.0|
| `hashtags` | Array of [Tag](#tag) | <i class="fa fa-times" aria-hidden="true"></i> |1.1.0|

## Status

|Attribute|Type|Nullable|Added in|
|---------|-----------|:------:|:------:|
| `id` | String | <i class="fa fa-times" aria-hidden="true"></i> |0.1.0|
| `uri` | String | <i class="fa fa-times" aria-hidden="true"></i> |0.1.0|
| `url` | String (URL) | <i class="fa fa-check" aria-hidden="true"></i> |0.1.0|
| `account` | [Account](#account) | <i class="fa fa-times" aria-hidden="true"></i> |0.1.0|
| `in_reply_to_id` | String | <i class="fa fa-check" aria-hidden="true"></i> |0.1.0|
| `in_reply_to_account_id` | String | <i class="fa fa-check" aria-hidden="true"></i> |1.0.0|
| `reblog` | [Status](#status) | <i class="fa fa-check" aria-hidden="true"></i> |0.1.0|
| `content` | String (HTML) | <i class="fa fa-times" aria-hidden="true"></i> |0.1.0|
| `created_at` | String (Datetime) | <i class="fa fa-times" aria-hidden="true"></i> |0.1.0|
| `emojis` | Array of [Emoji](#emoji) | <i class="fa fa-times" aria-hidden="true"></i> |2.0.0|
| `replies_count` | Number | <i class="fa fa-times" aria-hidden="true"></i> |2.5.0|
| `reblogs_count` | Number | <i class="fa fa-times" aria-hidden="true"></i> |0.1.0|
| `favourites_count` | Number | <i class="fa fa-times" aria-hidden="true"></i> |0.1.0|
| `reblogged` | Boolean | <i class="fa fa-check" aria-hidden="true"></i> |0.1.0|
| `favourited` | Boolean | <i class="fa fa-check" aria-hidden="true"></i> |0.1.0|
| `muted` | Boolean | <i class="fa fa-check" aria-hidden="true"></i> |1.4.0|
| `sensitive` | Boolean | <i class="fa fa-times" aria-hidden="true"></i> |0.9.9|
| `spoiler_text` | String | <i class="fa fa-times" aria-hidden="true"></i> |1.0.0|
| `visibility` | [String (Enum)](#visibility) | <i class="fa fa-times" aria-hidden="true"></i> |0.9.9|
| `media_attachments` | Array of [Attachment](#attachment) | <i class="fa fa-times" aria-hidden="true"></i> |0.6.0|
| `mentions` | Array of [Mention](#mention) | <i class="fa fa-times" aria-hidden="true"></i> |0.6.0|
| `tags` | Array of [Tag](#tag) | <i class="fa fa-times" aria-hidden="true"></i> |0.9.0|
| `card` | [Card](#card) | <i class="fa fa-check" aria-hidden="true"></i> |2.6.0|
| `poll` | [Poll](#poll) | <i class="fa fa-check" aria-hidden="true"></i> |2.8.0|
| `application` | [Application](#application) | <i class="fa fa-times" aria-hidden="true"></i> |0.9.9|
| `language` | String (ISO6391) | <i class="fa fa-check" aria-hidden="true"></i> |1.4.0|
| `pinned` | Boolean | <i class="fa fa-check" aria-hidden="true"></i> |1.6.0|

### Visibility

- `public`
- `unlisted`
- `private`
- `direct`

## ScheduledStatus

|Attribute|Type|Nullable|Added in|
|---------|-----------|:------:|:------:|
| `id` | String | <i class="fa fa-times" aria-hidden="true"></i> |2.7.0|
| `scheduled_at` | String (Datetime) | <i class="fa fa-times" aria-hidden="true"></i> |2.7.0|
| `params` | [StatusParams](#statusparams) | <i class="fa fa-times" aria-hidden="true"></i> |2.7.0|
| `media_attachments` | Array of [Attachment](#attachment) | <i class="fa fa-times" aria-hidden="true"></i> |2.7.0|

### StatusParams

|Attribute|Type|Nullable|Added in|
|---------|-----------|:------:|:------:|
| `text` | String | <i class="fa fa-times" aria-hidden="true"></i> |2.7.0|
| `in_reply_to_id` | String | <i class="fa fa-check" aria-hidden="true"></i> |2.7.0|
| `media_ids` | Array of String | <i class="fa fa-check" aria-hidden="true"></i> |2.7.0|
| `sensitive` | Boolean | <i class="fa fa-check" aria-hidden="true"></i> |2.7.0|
| `spoiler_text` | String | <i class="fa fa-check" aria-hidden="true"></i> |2.7.0|
| `visibility` | [String (Enum)](#visibility) | <i class="fa fa-times" aria-hidden="true"></i> |2.7.0|
| `scheduled_at` | String (Datetime) | <i class="fa fa-check" aria-hidden="true"></i> |2.7.0|
| `application_id` | String | <i class="fa fa-times" aria-hidden="true"></i> |2.7.0|

## Tag

|Attribute|Type|Nullable|Added in|
|---------|-----------|:------:|:------:|
| `name` | String | <i class="fa fa-times" aria-hidden="true"></i> |0.9.0|
| `url` | String (URL) | <i class="fa fa-times" aria-hidden="true"></i> |0.9.0|
| `history` | Array of [History](#history) | <i class="fa fa-check" aria-hidden="true"></i> |2.4.1|

### History

|Attribute|Type|Nullable|Added in|
|---------|-----------|:------:|:------:|
| `day` | String (UNIX timestamp) | <i class="fa fa-times" aria-hidden="true"></i> |2.4.1|
| `uses` | Number | <i class="fa fa-times" aria-hidden="true"></i> |2.4.1|
| `accounts` | Number | <i class="fa fa-times" aria-hidden="true"></i> |2.4.1|

## Conversation

|Attribute|Type|Nullable|Added in|
|---------|-----------|:------:|:------:|
| `id` | String  | <i class="fa fa-times" aria-hidden="true"></i> |2.6.0|
| `accounts` | Array of [Account](#account) | <i class="fa fa-times" aria-hidden="true"></i> |2.6.0|
| `last_status` | [Status](#status) | <i class="fa fa-check" aria-hidden="true"></i> |2.6.0|
| `unread` | Boolean | <i class="fa fa-times" aria-hidden="true"></i> |2.6.0|
