# About Soapbox Legacy

Soapbox Legacy is the older version of Soapbox, which is a direct fork of Gab's fork of Mastodon frontend and backend.
This repo is being deprecated and it is strongly advised to use the new Soapbox frontend with the Pleroma backend instead.

## Quick Navigation

- [Learn how to use Soapbox Legacy](usage/basics)
- [Learn how to install Soapbox Legacy](administration/installation)
- [Learn how to write an app for Soapbox Legacy](API/guidelines)

The docs for Soapbox Legacy are based on [Mastodon's docs](https://docs.joinmastodon.org) and are licensed under the [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) license.
