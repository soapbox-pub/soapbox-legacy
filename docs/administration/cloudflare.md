# Cloudflare

## Whitelist Cloudflare IPs

The IP address of your instance may be exposed to other instances during federation.
With the IP address, it's possible for attackers to circumvent Cloudflare and attack your server directly.
To prevent this, you can whitelist [Cloudflare IPs](https://www.cloudflare.com/ips/).

```sh
ufw allow from 173.245.48.0/20 to any port 80
ufw allow from 103.21.244.0/22 to any port 80
ufw allow from 103.22.200.0/22 to any port 80
ufw allow from 103.31.4.0/22 to any port 80
ufw allow from 141.101.64.0/18 to any port 80
ufw allow from 108.162.192.0/18 to any port 80
ufw allow from 190.93.240.0/20 to any port 80
ufw allow from 188.114.96.0/20 to any port 80
ufw allow from 197.234.240.0/22 to any port 80
ufw allow from 198.41.128.0/17 to any port 80
ufw allow from 162.158.0.0/15 to any port 80
ufw allow from 104.16.0.0/12 to any port 80
ufw allow from 172.64.0.0/13 to any port 80
ufw allow from 131.0.72.0/22 to any port 80
ufw allow from 2400:cb00::/32 to any port 80
ufw allow from 2606:4700::/32 to any port 80
ufw allow from 2803:f800::/32 to any port 80
ufw allow from 2405:b500::/32 to any port 80
ufw allow from 2405:8100::/32 to any port 80
ufw allow from 2a06:98c0::/29 to any port 80
ufw allow from 2c0f:f248::/32 to any port 80
ufw allow from 173.245.48.0/20 to any port 443
ufw allow from 103.21.244.0/22 to any port 443
ufw allow from 103.22.200.0/22 to any port 443
ufw allow from 103.31.4.0/22 to any port 443
ufw allow from 141.101.64.0/18 to any port 443
ufw allow from 108.162.192.0/18 to any port 443
ufw allow from 190.93.240.0/20 to any port 443
ufw allow from 188.114.96.0/20 to any port 443
ufw allow from 197.234.240.0/22 to any port 443
ufw allow from 198.41.128.0/17 to any port 443
ufw allow from 162.158.0.0/15 to any port 443
ufw allow from 104.16.0.0/12 to any port 443
ufw allow from 172.64.0.0/13 to any port 443
ufw allow from 131.0.72.0/22 to any port 443
ufw allow from 2400:cb00::/32 to any port 443
ufw allow from 2606:4700::/32 to any port 443
ufw allow from 2803:f800::/32 to any port 443
ufw allow from 2405:b500::/32 to any port 443
ufw allow from 2405:8100::/32 to any port 443
ufw allow from 2a06:98c0::/29 to any port 443
ufw allow from 2c0f:f248::/32 to any port 443
```

If you have existing rules for port `80` and `443`, you will want to remove them.
This way, traffic can only go through Cloudflare.

```sh
ufw delete allow 80
ufw delete allow 443
```

Run `ufw status` and ensure that only the Cloudflare rules and SSH are enabled.

## Get real IP address of users

Since only Cloudflare will be making HTTP requests to your server, all users will appear to be coming from a Cloudflare IP address when viewed through the Soapbox Legacy admin dashboard.
In order for Soapbox Legacy to record the real IP address of users, you will need to configure Nginx.

Edit `/etc/nginx/nginx.conf` to add the following directives:

```nginx
user www-data;
worker_processes auto;
...

http {
	...

	##
	# Cloudflare Real IP
	##

	set_real_ip_from 103.21.244.0/22;
	set_real_ip_from 103.22.200.0/22;
	set_real_ip_from 103.31.4.0/22;
	set_real_ip_from 104.16.0.0/12;
	set_real_ip_from 108.162.192.0/18;
	set_real_ip_from 131.0.72.0/22;
	set_real_ip_from 141.101.64.0/18;
	set_real_ip_from 162.158.0.0/15;
	set_real_ip_from 172.64.0.0/13;
	set_real_ip_from 173.245.48.0/20;
	set_real_ip_from 188.114.96.0/20;
	set_real_ip_from 190.93.240.0/20;
	set_real_ip_from 197.234.240.0/22;
	set_real_ip_from 198.41.128.0/17;
	set_real_ip_from 2400:cb00::/32;
	set_real_ip_from 2606:4700::/32;
	set_real_ip_from 2803:f800::/32;
	set_real_ip_from 2405:b500::/32;
	set_real_ip_from 2405:8100::/32;
	set_real_ip_from 2c0f:f248::/32;
	set_real_ip_from 2a06:98c0::/29;

	real_ip_header CF-Connecting-IP;
}

...
```

See here for more information: <a href="https://support.cloudflare.com/hc/en-us/articles/200170786-Restoring-original-visitor-IPs-Logging-visitor-IP-addresses-with-mod-cloudflare-" target="_blank"><em>Restoring original visitor IPs - Cloudflare</em></a>
