# Upgrading to a new release

When a new version of Soapbox Legacy comes out, it appears on the [GitLab releases page](https://gitlab.com/soapbox-pub/soapbox-legacy/releases). Please mind that running unreleased code from the `master` branch, while possible, is not recommended.

Soapbox Legacy releases correspond to git tags. First, switch to the `mastodon` user:

```sh
su - mastodon
```

And navigate to the Soapbox Legacy root directory:

```sh
cd /home/mastodon/live
```

Download the releases's code, assuming that the version is called `v2.5.0`:

```sh
git fetch --tags
git checkout v2.5.0
```

The release page contains a changelog, and below it, upgrade instructions. This is where you would execute them, for example, if the release mentions that you need to re-compile assets, you would execute:

```sh
RAILS_ENV=production bundle exec rails assets:precompile
```

After you have executed all special release-specific instructions, the last thing remaining is restarting Soapbox Legacy. *Usually* the streaming API is not updated, and therefore does not require a restart. Restarting the streaming API can lead to an unusually high load on the server, so it is advised to avoid it if possible.

Switch back to root:

```sh
exit
```

You would restart Sidekiq:

```sh
systemctl restart mastodon-sidekiq
```

And you would reload the web process to avoid downtime:

```sh
systemctl reload mastodon-web
```

**That's all!** You're running the new version of Soapbox Legacy now.
