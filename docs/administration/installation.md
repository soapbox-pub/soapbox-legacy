# Installation

## DNS

DNS records should be added before anything is done on the server.

The records added are:

* A record (IPv4 address) for example.com
* AAAA record (IPv6 address) for example.com

## Basic server setup (optional)

If you are setting up a fresh machine, it is recommended that you secure it first. Assuming that you are running **Ubuntu 18.04**:

### Do not allow password-based SSH login (keys only)

First make sure you are actually logging in to the server using keys and not via a password, otherwise this will lock you out. Many hosting providers support uploading a public key and automatically set up key-based root login on new machines for you.

Edit `/etc/ssh/sshd_config` and find `PasswordAuthentication`. Make sure it's uncommented and set to `no`. If you made any changes, restart sshd:

```sh
systemctl restart ssh
```

### Update system packages

```sh
apt update && apt upgrade -y
```

### Install fail2ban so it blocks repeated login attempts

```sh
apt install fail2ban
```

Edit `/etc/fail2ban/jail.local` and put this inside:

```ini
[DEFAULT]
destemail = your@email.here
sendername = Fail2Ban

[sshd]
enabled = true
port = 22

[sshd-ddos]
enabled = true
port = 22
```

Finally restart fail2ban:

```sh
systemctl restart fail2ban
```

### Configure firewall and only whitelist SSH, HTTP and HTTPS ports

UFW, or Uncomplicated Firewall, is a simple firewall already installed on your system.
By default it's disabled, so you should really enable it for better security.

You will want to deny incoming traffic and allow outgoing traffic by default.

```sh
ufw default deny incoming
ufw default allow outgoing
```

Next, allow SSH, HTTP, and HTTPS.

```sh
ufw allow ssh
ufw allow 80
ufw allow 443
```

Finally, enable UFW.

```sh
ufw enable
```

You can use `ufw status` to see the status of your firewall.
For more about UFW, see [this article](https://www.digitalocean.com/community/tutorials/how-to-set-up-a-firewall-with-ufw-on-ubuntu-18-04).

## Pre-requisites

- A machine running **Ubuntu 18.04** that you have root access to
- A **domain name** (or a subdomain) for the Soapbox Legacy server, e.g. `example.com`
- An e-mail delivery service or other **SMTP server**

You will be running the commands as root. If you aren't already root, switch to root:

```sh
sudo -i
```

### System repositories

Make sure curl is installed first:

```sh
apt install -y curl
```

#### Node.js

```sh
curl -sL https://deb.nodesource.com/setup_10.x | bash -
```

#### Yarn

```sh
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
```

### System packages

* **imagemagick** - Soapbox Legacy uses imagemagick for image related operations
* **ffmpeg** - Soapbox Legacy uses ffmpeg for conversion of GIFs to MP4s
* **libprotobuf-dev** and **protobuf-compiler** - Soapbox Legacy uses these for language detection
* **nginx** - nginx is our frontend web server
* **redis-\*** - Soapbox Legacy uses redis for its in-memory data structure store
* **postgresql-\*** - Soapbox Legacy uses PostgreSQL as its SQL database
* **nodejs** - Node is used for Soapbox Legacy's streaming API
* **yarn** - Yarn is a Node.js package manager
* **Other** -dev packages, g++ - these are needed for the compilation of Ruby using ruby-build.

```sh
apt update
apt install -y \
  imagemagick ffmpeg libpq-dev libxml2-dev libxslt1-dev file git-core \
  g++ libprotobuf-dev protobuf-compiler pkg-config nodejs gcc autoconf \
  bison build-essential libssl-dev libyaml-dev libreadline6-dev \
  zlib1g-dev libncurses5-dev libffi-dev libgdbm5 libgdbm-dev \
  nginx redis-server redis-tools postgresql postgresql-contrib \
  certbot python-certbot-nginx yarn libidn11-dev libicu-dev libjemalloc-dev
```

### Installing Ruby

We will be using rbenv to manage Ruby versions, because it's easier to get the right versions and to update once a newer release comes out. rbenv must be installed for a single Linux user, therefore, first we must create the user Soapbox Legacy will be running as:

```sh
adduser --disabled-login soapbox
```

We can then switch to the user:

```sh
su - soapbox
```

And proceed to install rbenv and rbenv-build:

```sh
git clone https://github.com/rbenv/rbenv.git ~/.rbenv
cd ~/.rbenv && src/configure && make -C src
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(rbenv init -)"' >> ~/.bashrc
exec bash
git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
```

Once this is done, we can install the correct Ruby version:

```sh
RUBY_CONFIGURE_OPTS=--with-jemalloc rbenv install 2.6.1
rbenv global 2.6.1
```

Default gem version shipped with ruby_2.6.0 is incompatible with latest bundler, so we need to update gem:

```
gem update --system
```

We'll also need to install bundler:

```sh
gem install bundler --no-document
```

Return to the root user:

```sh
exit
```

## Setup
### Setting up PostgreSQL
#### Performance configuration (optional)

For optimal performance, you may use [pgTune](https://pgtune.leopard.in.ua/#/) to generate an appropriate configuration and edit values in `/etc/postgresql/9.6/main/postgresql.conf` before restarting PostgreSQL with `systemctl restart postgresql`

#### Creating a user

You will need to create a PostgreSQL user that Soapbox Legacy could use. It is easiest to go with "ident" authentication in a simple setup, i.e. the PostgreSQL user does not have a separate password and can be used by the Linux user with the same username.

Open the prompt:

```sh
sudo -u postgres psql
```

In the prompt, execute:

```
CREATE USER soapbox CREATEDB;
\q
```

Done!

### Setting up Soapbox Legacy

It is time to download the Soapbox Legacy code. Switch to the soapbox user:

```sh
su - soapbox
```

#### Checking out the code

Use git to download the latest stable release of Soapbox Legacy:

```sh
git clone https://gitlab.com/soapbox-pub/soapbox.git live && cd live
```

#### Installing the last dependencies

Now to install Ruby and JavaScript dependencies:

```sh
bundle install \
  -j$(getconf _NPROCESSORS_ONLN) \
  --deployment --without development test
yarn install --pure-lockfile
```

#### Generating a configuration

Run the interactive setup wizard:

```sh
RAILS_ENV=production bundle exec rake gabsocial:setup
```

This will:

- Create a configuration file
- Run asset precompilation
- Create the database schema

The configuration file is saved as `.env.production`. You can review and edit it to your liking. Refer to the [documentation on configuration](../configuration).

You're done with the soapbox user for now, so switch back to root:

```sh
exit
```

### Setting up nginx

Copy the configuration template for nginx from the Soapbox Legacy directory:

```sh
cp /home/soapbox/live/dist/nginx.conf /etc/nginx/sites-available/soapbox
ln -s /etc/nginx/sites-available/soapbox /etc/nginx/sites-enabled/soapbox
```

Then edit `/etc/nginx/sites-available/soapbox` to replace `example.com` with your own domain name, and make any other adjustments you might need.

Reload nginx for the changes to take effect:

```sh
systemctl reload nginx
```

### Acquiring a SSL certificate

We'll use Let's Encrypt to get a free SSL certificate:

```sh
certbot --nginx -d example.com
```

This will obtain the certificate, automatically update `/etc/nginx/sites-available/soapbox` to use the new certificate, and reload nginx for the changes to take effect.

At this point you should be able to visit your domain in the browser and see the elephant hitting the computer screen error page. This is because we haven't started the Soapbox Legacy process yet.

### Setting up systemd services

Copy the systemd service templates from the Soapbox Legacy directory:

```sh
cp /home/soapbox/live/dist/soapbox-*.service /etc/systemd/system/
```

Then edit the files to make sure the username and paths are correct:

- `/etc/systemd/system/soapbox-web.service`
- `/etc/systemd/system/soapbox-sidekiq.service`
- `/etc/systemd/system/soapbox-streaming.service`

Finally, start and enable the new systemd services:

```sh
systemctl start soapbox-web soapbox-sidekiq soapbox-streaming
systemctl enable soapbox-*
```

They will now automatically start at boot time.

**Hurray! This is it. You can visit your domain in the browser now!**
