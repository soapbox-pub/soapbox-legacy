# frozen_string_literal: true

require 'stripe'

namespace :stripe do
  desc 'Populate Stripe with the required base data.'
  task :initialize => :environment do
    site_title = Setting.site_title

    Stripe::Product.create({
      id: 'prod_donation',
      name: 'Donation',
      statement_descriptor: site_title,
      type: 'service',
    })

    Stripe::Plan.create({
      amount: 1,
      interval: 'month',
      product: 'prod_donation',
      currency: 'usd',
      id: 'plan_monthly_donation',
      nickname: 'Monthly Donation',
    })
  end

  desc 'Calculate the total monthly donations.'
  task :calc => :environment do
    amount = 0
    patrons = 0

    subs = Stripe::Subscription.list({ plan: "plan_monthly_donation" })
    subs.auto_paging_each do |sub|
      amount += sub.quantity
      patrons += 1
    end

    Redis.current.set('funding:amount', amount)
    Redis.current.set('funding:patrons', patrons)

    puts "Receiving $#{'%.2f' % (amount/100.0)}/mo from #{patrons} patrons"
  end
end
