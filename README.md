# Soapbox Legacy

> :warning: **This project is being deprecated.**
The new way is to run [soapbox-fe](https://gitlab.com/soapbox-pub/soapbox-fe) on Pleroma.

---

Soapbox is a Mastodon-compatible social media software with a focus on custom branding and accessibility.

Soapbox is a fork of [Mastodon](https://github.com/tootsuite/mastodon), incorporating [Gab Social](https://code.gab.com/gab/social/gab-social)'s user interface.
It is free and open source, licensed under the terms and conditions of AGPL-3.0.

## Project philosophy

Soapbox is designed to be adapted by any community.
It's another option instead of Mastodon, Gab Social, or Pleroma.

### Custom branding

We believe that Fediverse servers are most successful when they appeal to a target demographic.
That's why custom branding is a major focus of this project.

Soapbox uses your server's name and logo throughout the interface.
While Mastodon is meant to be propped up with low effort, Soapbox expects that you have a vision.

Themes in a variety of colors are being developed.
For now, only purple is available.

### Federation interoperability

It's important to Soapbox that federation between servers remains intact.
Soapbox is more conservative about implementing new federation features.

Quote posting and groups are not yet implemented.
Quote posting will only be implemented if it federates between Soapbox servers.
Group posts are planned to be contained and local-only until a secure system for federation of private groups is implemented.

### Self-funding

A recurring donations platform is integrated directly into Soapbox.
For the long-term success of the Fediverse, it's important that instances can be funded by their users.
Soapbox makes it easy for users to donate to your instance, and it rewards them for doing so.

### Comparison with other software

Benefits over Mastodon:

* Custom branding.
* Familiar user interface.
* Integrated recurring donations platform.
* Customizable character limit (3000 default).

Benefits over Gab Social:

* Doesn't break federation.
* Community developed.
* Bug fixes.
* Profile customization for PRO accounts.

Disadvantages of Soapbox:

* Doesn't implement groups or quote posting. (Gab Social)
* No account migration. (Mastodon v3.0.0)
* More resource-intensive than Pleroma.
* Lacking thorough translations of non-English languages compared to Mastodon.
* Our team has less development resources than Mastodon or Gab.

## Deployment

**Tech stack:**

- **Ruby on Rails** powers the REST API and other web pages
- **React.js** and Redux are used for the dynamic parts of the interface
- **Node.js** powers the streaming API

**Requirements:**

- **PostgreSQL** 9.5+
- **Redis**
- **Ruby** 2.4+
- **Node.js** 8+

The repository includes deployment configurations for **Docker and docker-compose**, but also a few specific platforms like **Heroku**, **Scalingo**, and **Nanobox**.

A **stand-alone** installation guide will be provided as soon as possible.

### Configuring Stripe for donations

You will need to add these environment variables to `.env.production`:

* `STRIPE_SECRET_KEY`
* `STRIPE_PUBLIC_KEY`
* `STRIPE_SIGNING_SECRET`

For the Stripe signing secret, add a webhook with a URL like `https://mysite.com/webhooks/stripe` and enable all events.

On the server, run `bundle exec rake stripe:initialize` one time to load the data needed into Stripe.

## Local development

To get started developing on Soapbox, you will need to run a version of it locally.
The following instructions assume you are already familiar with using a terminal program.

1. Install [Vagrant](https://www.vagrantup.com/) and [VirtualBox](https://www.virtualbox.org/) if you haven't already.
2. Clone this repository with `git clone https://gitlab.com/soapbox-pub/soapbox.git`
3. Change into the project directory with `cd soapbox`
4. Run `vagrant up` to provision the virtual machine. This will take a while.
5. Finally, run `vagrant ssh -c "cd /vagrant && foreman start"` to start the local web server.
6. Visit http://0.0.0.0:3000 in your web browser to see Soapbox's splash screen. If it doesn't load, or styling is missing, wait another minute and refresh the page.
7. Log in with the username `admin` and password `administrator`
8. Have fun developing on Soapbox!

## License

Copyright (C) 2016-2019 Eugen Rochko, Gab AI, Inc., and other contributors

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
