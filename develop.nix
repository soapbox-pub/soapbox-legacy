# Use this expression with nix-shell to create a Soapbox development
# environment with the pinned version of Nixpkgs.
let
  nixpkgs = import ./dist/nixpkgs/pinned-nixpkgs.nix;
  pkgs = import nixpkgs {
    config = { };
    overlays = [
      (self: super: {
        soapbox = self.callPackage ./dist/nixpkgs/soapbox { };
      })
    ];
  };
in pkgs.soapbox.overrideAttrs (oldAttrs: rec {
  buildPhase = "";   dontBuild = true;
  installPhase = ""; dontInstall = true;
  propagatedBuildInputs = oldAttrs.propagatedBuildInputs ++ [
    pkgs.goreman pkgs.yarn pkgs.nodejs oldAttrs.passthru.gems
  ];
  # Soapbox's developer tools expect to find a writable node_modules
  # directory.
  shellHook = ''
    if [[ -e node_modules && -d node_modules ]]; then
      echo "Refreshing node_modules directory"
    else
      echo "Creating node_modules directory"
    fi
    rm -rf node_modules
    cp -r "${oldAttrs.passthru.js-modules}/libexec/soapbox/node_modules" node_modules
    chmod -R u+w node_modules
  '';
})
