# frozen_string_literal: true

require 'rails_helper'
require 'pundit/rspec'

RSpec.describe AccountPolicy do
  let(:subject)   { described_class }
  let(:admin)     { Fabricate(:user, admin: true).account }
  let(:moderator) { Fabricate(:user, moderator: true).account }
  let(:john)      { Fabricate(:user).account }
  let(:jane)      { Fabricate(:user).account }
  let(:bart)      { Fabricate(:account, domain: 'foreign.tld') }

  permissions :index?, :show?, :unsuspend?, :unsilence?, :remove_avatar?, :remove_header? do
    context 'staff' do
      it 'permits' do
        expect(subject).to permit(admin)
        expect(subject).to permit(moderator)
      end
    end

    context 'not staff' do
      it 'denies' do
        expect(subject).to_not permit(john)
        expect(subject).to_not permit(bart)
      end
    end
  end

  permissions :redownload?, :subscribe?, :unsubscribe? do
    context 'admin' do
      it 'permits' do
        expect(subject).to permit(admin)
      end
    end

    context 'not admin' do
      it 'denies' do
        expect(subject).to_not permit(moderator)
        expect(subject).to_not permit(john)
        expect(subject).to_not permit(bart)
      end
    end
  end

  permissions :suspend? do
    let(:other_admin)     { Fabricate(:user, admin: true).account }
    let(:other_moderator) { Fabricate(:user, moderator: true).account }

    context 'staff' do
      context 'record is staff' do
        it 'denies' do
          expect(subject).to_not permit(admin, other_admin)
          expect(subject).to_not permit(admin, other_moderator)
          expect(subject).to_not permit(moderator, other_admin)
          expect(subject).to_not permit(moderator, other_moderator)
        end
      end
    end

    context 'admin' do
      context 'record is non-staff local' do
        it 'permits' do
          expect(subject).to permit(admin, john)
        end
      end

      context 'record is remote' do
        it 'permits' do
          expect(subject).to permit(admin, bart)
        end
      end
    end

    context 'moderator' do
      context 'record is non-staff local' do
        it 'denies' do
          expect(subject).to_not permit(moderator, john)
        end
      end

      context 'record is remote' do
        it 'permits' do
          expect(subject).to permit(moderator, bart)
        end
      end
    end

    context 'not staff' do
      it 'denies' do
        expect(subject).to_not permit(john, jane)
        expect(subject).to_not permit(bart, jane)
      end
    end
  end

  permissions :silence? do
    let(:other_admin)     { Fabricate(:user, admin: true).account }
    let(:other_moderator) { Fabricate(:user, moderator: true).account }

    context 'staff' do
      context 'record is staff' do
        it 'denies' do
          expect(subject).to_not permit(admin, other_admin)
          expect(subject).to_not permit(admin, other_moderator)
          expect(subject).to_not permit(moderator, other_admin)
          expect(subject).to_not permit(moderator, other_moderator)
        end
      end

      context 'record is not staff' do
        it 'permits' do
          expect(subject).to permit(admin, john)
          expect(subject).to permit(admin, bart)
          expect(subject).to permit(moderator, john)
          expect(subject).to permit(moderator, bart)
        end
      end
    end

    context 'not staff' do
      it 'denies' do
        expect(subject).to_not permit(john, jane)
        expect(subject).to_not permit(bart, jane)
      end
    end
  end

  permissions :memorialize? do
    let(:other_admin) { Fabricate(:user, admin: true).account }

    context 'admin' do
      context 'record is admin' do
        it 'denies' do
          expect(subject).to_not permit(admin, other_admin)
        end
      end

      context 'record is not admin' do
        it 'permits' do
          expect(subject).to permit(admin, john)
        end
      end
    end

    context 'not admin' do
      it 'denies' do
        expect(subject).to_not permit(john, jane)
      end
    end
  end

  permissions :verify?, :edit_pro?, :update_badges? do
    let(:other_admin)     { Fabricate(:user, admin: true).account }
    let(:other_moderator) { Fabricate(:user, moderator: true).account }

    context 'admin' do
      it 'permits' do
        expect(subject).to permit(admin, john)
        expect(subject).to permit(admin, other_admin)
        expect(subject).to permit(admin, other_moderator)
      end
    end

    context 'moderator' do
      it 'denies' do
        expect(subject).to_not permit(moderator, john)
        expect(subject).to_not permit(moderator, other_admin)
        expect(subject).to_not permit(moderator, other_moderator)
      end
    end

    context 'not staff' do
      it 'denies' do
        expect(subject).to_not permit(john, jane)
        expect(subject).to_not permit(bart, jane)
      end
    end
  end
end
